package com.example.taskifydemo.dto.response;

import com.example.taskifydemo.model.TaskStatus;
import com.example.taskifydemo.model.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class TaskForUsersResponseDto {
    Long id;
    String title;
    String description;
    @JsonFormat(pattern = "dd.MM.yyyy")
    LocalDate deadline;
    @Enumerated(EnumType.STRING)
    TaskStatus status;
    @Builder.Default
    List<User> userList=new ArrayList<>();
}
