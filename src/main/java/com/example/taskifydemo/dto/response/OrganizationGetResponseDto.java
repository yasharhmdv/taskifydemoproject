package com.example.taskifydemo.dto.response;

import com.example.taskifydemo.model.User;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class OrganizationGetResponseDto {
    Long id;
    String name;
    String confirmationCode;
    @Builder.Default
    List<User> users = new ArrayList<>();
}
