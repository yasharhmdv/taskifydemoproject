package com.example.taskifydemo.dto.response;

import com.example.taskifydemo.model.Address;
import com.example.taskifydemo.model.PhoneNumber;
import com.example.taskifydemo.model.Task;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class UserResponseDto {
    Long id;
    String name;
    String surname;
    List<PhoneNumber> phoneNumbers;
    Address address;
    @Builder.Default
    List<Task> taskList = new ArrayList<>();
}


