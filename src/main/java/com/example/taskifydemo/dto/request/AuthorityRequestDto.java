package com.example.taskifydemo.dto.request;

import com.example.taskifydemo.model.UserAuthority;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthorityRequestDto {
    @Enumerated(EnumType.STRING)
    UserAuthority authority;
}
