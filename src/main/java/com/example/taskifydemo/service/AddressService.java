package com.example.taskifydemo.service;

import com.example.taskifydemo.dto.request.AddressRequestDto;
import com.example.taskifydemo.dto.response.AddressResponseDto;
import com.example.taskifydemo.model.Address;
import com.example.taskifydemo.repository.AddressRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AddressService {

    private final AddressRepository addressRepository;
    private final ModelMapper modelMapper;


    public AddressResponseDto create(AddressRequestDto requestDto) {
        Address address = Address.builder()
                .address(requestDto.getAddress())
                .build();
        Address savedAddress = addressRepository.save(address);
        return modelMapper.map(savedAddress, AddressResponseDto.class);
    }

    public AddressResponseDto get(Long id) {
        Address address = addressRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Address with id %s not found", id)));
        return modelMapper.map(address, AddressResponseDto.class);
    }

    public List<AddressResponseDto> getAll() {
        List<Address> addressList = addressRepository.findAll();
        List<AddressResponseDto> addressResponseDtoList = new ArrayList<>();
        addressList.forEach(address ->
                addressResponseDtoList.add(modelMapper.map(address, AddressResponseDto.class)));
        return addressResponseDtoList;
    }


    public AddressResponseDto update(Long id, AddressRequestDto requestDto) {
        Address address = addressRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Address with id %s not found", id)));

        Address addressForUpdate = modelMapper.map(requestDto, Address.class);
        addressForUpdate.setId(id);

        return modelMapper.map(addressRepository.save(addressForUpdate), AddressResponseDto.class);
    }


    public AddressResponseDto delete(Long id) {
        Address address = addressRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Address with id %s not found", id)));

        addressRepository.deleteById(id);
        return modelMapper.map(address, AddressResponseDto.class);
    }

}
