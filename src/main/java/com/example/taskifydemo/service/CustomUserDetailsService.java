package com.example.taskifydemo.service;

import com.example.taskifydemo.model.User;
import com.example.taskifydemo.security.UserImpls;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class  CustomUserDetailsService implements UserDetailsService {
    private final AuthService authService;

    @Lazy
    public CustomUserDetailsService(AuthService authService) {
        this.authService = authService;
    }

    public UserDetails loadUserByUsername(String email) {
        User user = authService.findUser(email);

        return UserImpls.create(user);
    }
}
