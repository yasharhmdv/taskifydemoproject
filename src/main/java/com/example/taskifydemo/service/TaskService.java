package com.example.taskifydemo.service;

import com.example.taskifydemo.dto.request.TaskCreateRequestDto;
import com.example.taskifydemo.dto.request.TaskUpdateRequestDto;
import com.example.taskifydemo.dto.request.UsersListDto;
import com.example.taskifydemo.dto.response.TaskForUsersResponseDto;
import com.example.taskifydemo.dto.response.TaskResponseDto;
import com.example.taskifydemo.model.Task;
import com.example.taskifydemo.model.TaskStatus;
import com.example.taskifydemo.model.User;
import com.example.taskifydemo.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class TaskService {

    private final TaskRepository taskRepository;
    private final ModelMapper modelMapper;


    public TaskResponseDto create(TaskCreateRequestDto requestDto) {
        Task newTask = Task.builder()
                .title(requestDto.getTitle())
                .description(requestDto.getDescription())
                .deadline(requestDto.getDeadline())
                .status(TaskStatus.TO_DO)
                .build();
        Task savedTask = taskRepository.save(newTask);
        return modelMapper.map(savedTask, TaskResponseDto.class);
    }

    public TaskResponseDto get(Long id) {
        Task task = taskRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Task with id %s not found", id)));
        return modelMapper.map(task, TaskResponseDto.class);
    }

    public List<TaskResponseDto> getAllTasksByOrganizationId(Long organizationId) {
        List<Task> taskList = taskRepository.findAllTasksByOrganizationId(organizationId);
        List<TaskResponseDto> taskResponseDtoList = new ArrayList<>();
        taskList.forEach(task ->
                taskResponseDtoList.add(modelMapper.map(task, TaskResponseDto.class)));
        return taskResponseDtoList;
    }

    public List<TaskResponseDto> getAllTasksByUserId(Long organizationId) {
        List<Task> taskList = taskRepository.findAllTasksByUsersId(organizationId);
        List<TaskResponseDto> taskResponseDtoList = new ArrayList<>();
        taskList.forEach(task ->
                taskResponseDtoList.add(modelMapper.map(task, TaskResponseDto.class)));
        return taskResponseDtoList;
    }


    public TaskResponseDto update(Long id, TaskUpdateRequestDto requestDto) {
        Task task = taskRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Task with id %s not found", id)));

        Task taskForUpdate = modelMapper.map(requestDto, Task.class);
        taskForUpdate.setId(id);

        return modelMapper.map(taskRepository.save(taskForUpdate), TaskResponseDto.class);
    }

    public TaskForUsersResponseDto assignUsersToTask(Long taskId, UsersListDto listDto) {
        Task task = taskRepository.findById(taskId).orElseThrow(() ->
                new RuntimeException(String.format("Task with id %s not found", taskId)));
        for (User user : listDto.getUserList()) {
            user.getTasks().add(task);
        }
        TaskForUsersResponseDto taskForUsersResponseDto = modelMapper.map(task, TaskForUsersResponseDto.class);
        taskForUsersResponseDto.setUserList(listDto.getUserList());
        return taskForUsersResponseDto;
    }

    public TaskResponseDto delete(Long id) {
        Task task = taskRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Task with id %s not found", id)));

        taskRepository.deleteById(id);
        return modelMapper.map(task, TaskResponseDto.class);
    }

}
