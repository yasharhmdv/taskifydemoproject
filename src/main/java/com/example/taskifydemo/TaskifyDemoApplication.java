package com.example.taskifydemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskifyDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskifyDemoApplication.class, args);
    }

}
