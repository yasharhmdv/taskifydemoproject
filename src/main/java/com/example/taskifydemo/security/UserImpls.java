package com.example.taskifydemo.security;

import com.example.taskifydemo.model.User;
import com.example.taskifydemo.util.Util;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserImpls implements UserDetails {
    Long id;
    String name;
    @JsonIgnore
    String email;
    @JsonIgnore
    String password;
    Collection<? extends GrantedAuthority> authorities;

    public static UserImpls create(User user) {

        return UserImpls.builder()
                .id(user.getId())
                .name(Util.formatNameSurname(user.getName(), user.getSurname()))
                .email(user.getEmail())
                .password(user.getPassword())
                .authorities(user.getAuthorities())
                .build();
    }

    public static UserImpls create(Long id) {
        return UserImpls.builder()
                .id(id)
                .build();
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}