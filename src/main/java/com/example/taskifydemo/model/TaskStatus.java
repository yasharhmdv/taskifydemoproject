package com.example.taskifydemo.model;

import lombok.Getter;

@Getter
public enum TaskStatus {
    DELETED,
    TO_DO,
    IN_PROGRESS,
    COMPLETE;

}
