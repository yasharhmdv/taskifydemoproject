package com.example.taskifydemo.model;

public enum UserAuthority {
    ADMIN,
    ORGANIZATION_ADMIN,
    USER
}
