package com.example.taskifydemo.repository;

import com.example.taskifydemo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "select u from User u join fetch u.authorities a where u.email= :email")
    Optional<User> findUserByEmail(String email);

    Optional<User> findByEmail(String email);

    @Query(value = "select u from User u" +
            " left join u.address a" +
            " left join u.tasks t" +
            " where u.organization.id=:id")
    List<User> findAllUsers(Long id);


}
