package com.example.taskifydemo.repository;

import com.example.taskifydemo.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query(value = "select t from Task t" +
            " join User u" +
            " join u.organization o" +
            " where o.id=:id")
    List<Task> findAllTasksByOrganizationId(Long id);

    @Query(value = "select t from Task t" +
            " join fetch User u" +
            " where u.id=:id")
    List<Task> findAllTasksByUsersId(Long id);
}
