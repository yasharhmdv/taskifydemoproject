package com.example.taskifydemo.repository;

import com.example.taskifydemo.model.Authority;
import com.example.taskifydemo.model.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    Optional<Authority> findByAuthority(UserAuthority userAuthority);

}
