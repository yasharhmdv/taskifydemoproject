package com.example.taskifydemo.controller;

import com.example.taskifydemo.dto.request.OrganizationRequestDto;
import com.example.taskifydemo.dto.response.OrganizationResponseDto;
import com.example.taskifydemo.service.OrganizationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/organization")
@RequiredArgsConstructor
@Slf4j
public class OrganizationController {
    private final OrganizationService organizationService;


    @PostMapping("/new")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<OrganizationResponseDto> create(@RequestBody OrganizationRequestDto requestDto) {
        return ResponseEntity.ok(organizationService.create(requestDto));
    }


    @GetMapping("/gets")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<OrganizationResponseDto>> getAll() {
        return ResponseEntity.ok(organizationService.getAll());
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ORGANIZATION_ADMIN')")
    public ResponseEntity<OrganizationResponseDto> delete(@PathVariable Long id) {
        return ResponseEntity.ok(organizationService.delete(id));
    }

}
