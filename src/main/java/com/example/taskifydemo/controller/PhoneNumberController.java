package com.example.taskifydemo.controller;

import com.example.taskifydemo.dto.request.PhoneNumberRequestDto;
import com.example.taskifydemo.dto.response.PhoneNumberResponseDto;
import com.example.taskifydemo.service.PhoneNumberService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/phoneNumber")
@Slf4j
public class PhoneNumberController {
    private final PhoneNumberService phoneNumberService;


    @PostMapping("/new")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<PhoneNumberResponseDto> create(@RequestBody PhoneNumberRequestDto dto) {
        return ResponseEntity.ok(phoneNumberService.create(dto));
    }


    @GetMapping("/{id}")
    public ResponseEntity<PhoneNumberResponseDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(phoneNumberService.get(id));
    }


    @GetMapping("/gets")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<PhoneNumberResponseDto>> getAll() {
        return ResponseEntity.ok(phoneNumberService.getAll());
    }


    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ORGANIZATION_ADMIN','USER')")
    public ResponseEntity<PhoneNumberResponseDto> update(@PathVariable Long id, @RequestBody PhoneNumberRequestDto requestDto) {
        return ResponseEntity.ok(phoneNumberService.update(id, requestDto));
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ORGANIZATION_ADMIN','USER')")
    public ResponseEntity<PhoneNumberResponseDto> delete(@PathVariable Long id) {
        return ResponseEntity.ok(phoneNumberService.delete(id));
    }

}
