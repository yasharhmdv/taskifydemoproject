package com.example.taskifydemo.controller;

import com.example.taskifydemo.dto.request.UserCreateRequestDto;
import com.example.taskifydemo.dto.request.UserRequestDto;
import com.example.taskifydemo.dto.response.UserCreateResponseDto;
import com.example.taskifydemo.dto.response.UserResponseDto;
import com.example.taskifydemo.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@Slf4j
public class UserController {
    private final UserService userService;

    @PostMapping("/new")
    @PreAuthorize("hasAuthority('ORGANIZATION_ADMIN')")
    public ResponseEntity<UserCreateResponseDto> create(@RequestBody UserCreateRequestDto dto) {
        return ResponseEntity.ok(userService.create(dto));
    }


    @GetMapping("/{userId}")
    public ResponseEntity<UserResponseDto> get(@PathVariable Long userId) {
        return ResponseEntity.ok(userService.get(userId));
    }

    @GetMapping("/{organizationId}/gets")
    public ResponseEntity<List<UserResponseDto>> getAll(@PathVariable Long organizationId) {
        return ResponseEntity.ok(userService.getAll(organizationId));
    }


    @PutMapping("/{userId}")
    @PreAuthorize("hasAnyAuthority('ORGANIZATION_ADMIN','USER')")
    public ResponseEntity<UserResponseDto> update(@PathVariable Long userId, @RequestBody UserRequestDto requestDto) {
        return ResponseEntity.ok(userService.update(userId, requestDto));
    }


    @DeleteMapping("/{userId}")
    @PreAuthorize("hasAnyAuthority('ORGANIZATION_ADMIN','USER')")
    public ResponseEntity<UserResponseDto> delete(@PathVariable Long userId) {
        return ResponseEntity.ok(userService.delete(userId));
    }

}
