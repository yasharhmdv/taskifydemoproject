package com.example.taskifydemo.controller;

import com.example.taskifydemo.dto.request.AuthorityRequestDto;
import com.example.taskifydemo.dto.response.AuthorityResponseDto;
import com.example.taskifydemo.service.AuthorityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/authority")
@RequiredArgsConstructor
@Slf4j
public class AuthorityController {
    private final AuthorityService authorityService;

    @PostMapping("/new")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<AuthorityResponseDto> create(@RequestBody AuthorityRequestDto requestDto) {
        return ResponseEntity.ok(authorityService.create(requestDto));
    }


    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<AuthorityResponseDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(authorityService.get(id));
    }


    @GetMapping("/gets")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<AuthorityResponseDto>> getAll() {
        return ResponseEntity.ok(authorityService.getAll());
    }


    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<AuthorityResponseDto> update(@PathVariable Long id, @RequestBody AuthorityRequestDto requestDto) {
        return ResponseEntity.ok(authorityService.update(id, requestDto));
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<AuthorityResponseDto> delete(@PathVariable Long id) {
        return ResponseEntity.ok(authorityService.delete(id));
    }
}
